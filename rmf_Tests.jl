# Ellen Le May 1, 2015
# implements some random matrix factorization techniques
# Ref: rmf.jl in randommatrixfactorization repo and Halko paper


# Algo 4.1 Range Finder - finds an orthonormal basis with l columns to approx. range of A
function rangefinder(A,l)
	m = size(A, 1)
	n = size(A, 2)
	Omega = randn(n, l)
  Y = A*Omega
  Q,R = qr(Y)
  return Q
end


function test_rangefinder(n, m)
	A = Array(Float64, (n, n))
	range = randn(n, m)
	for i = 1:n
		sum = zeros(n)
		for j = 1:m
			sum += randn() * range[:, j]
		end
		A[:, i] = sum
	end
	Q = rangefinder(A)
	println("$(m) should be approximately equal to $(size(Q, 2))")
	println("$(norm(A - Q * ctranspose(Q) * A)) should be approximately zero")
end

test_rangefinder(1000, 100)

