__author__ = 'msingh'
import matplotlib.pyplot as plt
import numpy as np
from sklearn.datasets import make_regression

from .randnla.rand_lr import error, RandLR
from .randnla.rand_mul import RandMul
from .randnla.matrix_factorization import CX, CUR, cur

def relative_error(matrix, approx):
    return np.linalg.norm(matrix - approx, 'fro') / np.linalg.norm(matrix, ord='fro')

if __name__ == '__main__':
    A = np.random.randn(200, 20)
    B = np.random.randn(20, 50)
    for i in range(1, 20):
        #c = 20
        mul = RandMul(i)
        naive = mul.mul(A, B)
        muls = []
        for _ in range(200):
            s_mul = mul.mul(A, B, strategy='random_s_sampling')
            muls.append(s_mul)
        mean_mul = np.mean(muls)
        #print 'relative error ', relative_error(naive, mean_mul)
    X, y = make_regression(n_samples=1000, n_features=150)

    randlr = RandLR(X, y)
    weights = randlr.solve(200)
    y_pred = X.dot(weights)
    print 'error: ', error(y, y_pred)
    matrix = X

    x_axis = []
    errors = []
    """
    for c in range(1, 130):
        k = c + 20
        cx = CX(matrix, k=k)
        x_axis.append(c)
        factors = cx.factorize(c=c)
        e = relative_error(X, np.dot(factors['C'], factors['X']))
        errors.append(e)
        print 'CX factors error: ', e
    #plt.plot(x_axis, errors)
    #plt.show()
    """
    cur = CUR(X, 20)
    factors = cur.factorize(15, 12)
    print relative_error(X, np.dot(factors['C'], np.dot(factors['U'], factors['R'])))
