__author__ = 'msingh'

import numpy as np
from .rand_sampling import LeverageScoreSampling



class MatrixFactorization(object):
    def __init__(self, matrix):
        self.matrix = matrix
        self.m, self.n = matrix.shape

    def factorize(self, c):
        raise NotImplementedError


class CX(MatrixFactorization):
    def __init__(self, matrix, k):
        super(CX, self).__init__(matrix)
        self.k = k

    def factorize(self, c):
        col_lev_scores = self.leverage_scores
        bins = np.add.accumulate(col_lev_scores)
        indices = np.digitize(np.random.random_sample(c), bins)
        C = np.dot(self.matrix[:, indices], np.diag(1 / col_lev_scores[indices]))
        X = np.linalg.pinv(C).dot(self.matrix)
        return {'C': C, 'X': X}

    @property
    def leverage_scores(self):
        leverage_scores = LeverageScoreSampling(self.matrix, self.k)
        return leverage_scores.column_lev_scores


class CUR(MatrixFactorization):
    def __init__(self, matrix, k):
        super(CUR, self).__init__(matrix)
        self.k = k

    def _sample(self, probs, size):
        bins = np.add.accumulate(probs)
        indices = np.digitize(np.random.random_sample(size), bins)
        return indices

    def factorize(self, c, r):
        clev = LeverageScoreSampling(self.matrix, self.k).column_lev_scores
        cindices = self._sample(clev, c)
        C = np.dot(self.matrix[:, cindices], np.diag(1 / clev[cindices]))
        rlev = LeverageScoreSampling(C, c).column_lev_scores
        rindices = self._sample(rlev, r)
        R = np.diag(1 / rlev[rindices]).dot(self.matrix[rindices, :])
        W = np.diag(1 / rlev[rindices]).dot(C[rindices, :])
        U = np.linalg.pinv(W)
        return {'C': C, 'U': U, 'R': R}
